<?php

namespace ChrMue\cm_GoogleMaps;

class cm_GoogleMap_lib {

  public static function getMapTypeStr($type)
  {
	switch($type) {
		case 'n':
			return "ROADMAP"; //"G_NORMAL_MAP";
			break;
		case 's':
			return "SATELLITE"; //"G_SATELLITE_MAP";
			break;
		case 'h':
			return "HYBRID"; //"G_HYBRID_MAP";
			break;
		case 'p':
			return "TERRAIN"; //"G_PHYSICAL_MAP";
			break;
		default:
			return "ROADMAP";
			break;
	}
  }

  public static function getCtrlTypeStr($type)
  {
	switch ($type) {
		case 'hb':
			return 'HORIZONTAL_BAR';
			break;
		case 'dm':
			return 'DROPDOWN_MENU';
			break;
		case 'df':
		default:
			return 'DEFAULT';
			break;
	}
  }

  public static function getCtrlNavStr($type)
  {
    switch ($type) {
      case 'sm':
        return 'SMALL';
        break;
      case 'zo':
        return 'ZOOM_PAN';
        break;
      case 'an':
        return 'ANDROID';
        break;
      case 'df':
      default:
      	return 'DEFAULT';
      	break;
    }
  }

  public static function getCtrlZoomStr($type)
  {
    switch ($type) {
      case 'sm':
        return 'SMALL';
        break;
      case 'lg':
        return 'LARGE';
        break;
       case 'df':
      default:
      	return 'DEFAULT';
      	break;
    }
  }
  
  public static function validateCoordsExact($coords) {
    return (preg_match('/^([-]{0,1}\d+(\.\d*){0,1},[-]{0,1}\d+(\.\d*){0,1})$/', $coords));
  }

/**
 * calculate Distanz between two locations
 * 1. location: lat1,lng1
 * 2. location: coords
 */

  public static function getDistanceFormula($lat1, $lng1)
  {
    if (!is_numeric($lat1) || !is_numeric($lng1))
        return '"-"';

    $lat1 = deg2rad($lat1);
    $lng1 = deg2rad($lng1);
    $sinLat1 = sin($lat1);
    $cosLat1 = cos($lat1);
    $latitude = 'RADIANS(cm_googlemaps_lat)';
    $longitude = 'RADIANS(cm_googlemaps_lng)';

    /*
     $lat1 *=$this->factor;
     $lng1 *=$this->factor;
     $latitude *=$this->factor;
     $longitude *=$this->factor;
     */

    //  $formula= '6371*ACOS(SIN('.$lat1.')*SIN('.$latitude.')+COS('.$lat1.')*COS('.$latitude.')*COS('.$longitude.'-'.$lng1.'))';
    $formula = '6371*ACOS(' . $sinLat1 . '*SIN(' . $latitude . ')+' . $cosLat1 . '*COS(' . $latitude . ')*COS(' . $longitude . '-' . $lng1 . '))';

    return $formula;
  }
  
  public static function getDistanceOptionsHTML($distancevalues,$selectedItem)
  {
      if (preg_match('/^((?P<v1>\d+),)*\[(?P<std>\d+)\](,(?P<v2>\d+))*$/',
          $distancevalues))
      {
          //get default (preselected dropdown options)
          preg_match_all('/\[(?P<std>\d+)\]/', $distancevalues, $varValueArr);
          
          $std_dist = intval($varValueArr['std'][0]);
          
          //get dropdown options
          preg_match_all('/(?P<v1>\d+)/', $distancevalues, $varValueArr);
          
          $distOptions = '';
          $itemFound = false;
          foreach ($varValueArr['v1'] as $value)
          {
              if (intval($value) == $max_dist)
              {
                  $itemFound = true;
              }
          }
          if (!$itemFound)
          {
              $max_dist = $std_dist;
          }
          if ($selectedItem)
          {
              $max_dist =$selectedItem;
          }
          foreach ($varValueArr['v1'] as $value)
          {
              
              $val = intval($value);
              $distOptions .= '<option value="' . $val . '" ';
              if ($val == $max_dist)
              {
                  $itemForund = true;
                  $distOptions .= ' selected="selected"';
              }
              $distOptions .= '>' . $val . '</option>';
          }
          return $distOptions;
      }
      return null;
  }
  
  public function addGeoCoordsRegexp($strRegexp, $varValue, \Widget $objWidget)
  {
      if ($strRegexp == 'geocoords')
      {
          if (!cm_GoogleMap_lib::validateCoordsExact($varValue))
          {
              $objWidget->addError(sprintf($GLOBALS['TL_LANG']['tl_module']['cm_map_coorderr'],$objWidget->label));
          }
          return true;
      }
  }
  
  public function addIntvalsAndDefaultRegexp($strRegexp, $varValue, \Widget $objWidget)
  {
      
      //  die("xxx");
      if ($strRegexp == 'intvalsanddefault')
      {
          if (!preg_match('/^(\d+,)*\[\d+\](,\d+)*$/', $varValue))
          {
              $objWidget->addError(sprintf($GLOBALS['TL_LANG']['tl_module']['cm_map_intvalsanddefaulterr'],$objWidget->label));
          }
          return true;
      }
      return false;
  }
  
  
  public static function getBaseScript($ssl,$language,$apiKey)
  {

    $code=($ssl?'https://':'http://').'maps.googleapis.com/maps/api/js'.'?language='.$language.($apiKey?('&key='.$apiKey):'');
 	return $code;
  }

  public static function getMarkerBaseScript($ssl,$click,$clickType,$stayBubbleOpened=false)
  {
    $code ='
  // Function will be executed after page load 
  var cm_active_marker=null;
  var cm_active_info=null;
  function getMarker(url,pkt,txt,color,myIcon,map,clickable,isDefault) {
    var info;
    var markerIcon;
    var marker;';
/*
          .'markerImg = new google.maps.MarkerImage();'
*/
    $code .='
    if (myIcon != "") {
      var markerImg = new google.maps.MarkerImage(myIcon);
      var markerShadow = new google.maps.MarkerImage("");

      marker = new google.maps.Marker({
        icon:markerImg,
        position:pkt,
        map:map
      });
    }
    else
    {
      if (color!="") {
        markerImg = new google.maps.MarkerImage("'.($ssl?'https://':'http://').'www.google.com/intl/en_us/mapfiles/ms/micons/"+color+"-dot.png");
//          markerIcon.iconSize = new GSize(32,32);
        marker = new google.maps.Marker({
          icon:markerImg,
          position:pkt,
          map:map
        });
      }
      else {
        marker = new google.maps.Marker({
          position:pkt,
          map:map
        });
      }
    }
    info = new google.maps.InfoWindow({
        content: txt
    });
	
    if (clickable) {
      google.maps.event.addListener(marker, "'.(($stayBubbleOpened && $clickType==2)?'dblclick':'click').'", function() {
        window.location.href=url
      });
    }';
   
   
    
if (!$click) {

  if ($stayBubbleOpened)
  {
    $code .='   
      google.maps.event.addListener(marker, "'.($clickType==2?'click':'mouseover').'", function() {
        if (cm_active_marker!=null)
        {
          cm_active_info.close(map,cm_active_marker);
        }
        cm_active_info=info;  
        cm_active_marker=marker;  
        info.open(map,marker);
      });'; 
  }
  else
  {  $code .='   
      google.maps.event.addListener(marker, "mouseover", function() {
        info.open(map,marker);
      });
      google.maps.event.addListener(marker, "mouseout", function() {
        info.close(map,marker);
      });';
  }
}
else {
  $code .='   
    google.maps.event.addListener(marker, "click", function() {
        info.open(map,marker);
    });';
}

$code .='
  if (isDefault) {
    cm_active_marker=marker;
    cm_active_info=info;

  }
  return marker;
  }';
    return $code;
  }

  public static function getGoogleMapsGeoData($location,$ssl,$country='',$key='')
  {
    $varValue="";
    // Prepare request to Google maps
    $GoogleMapsXML= new GoogleMapsXMLData();
//	die("ssl: ".($ssl?"true":"false"));
    $GoogleMapsXML->setPath('maps.google.com',$ssl,'/maps/api/geocode/',($key?'&key='.$key:''));
    //$GoogleMapsXML->setPath('maps.googleapis.com',$ssl,'/maps/api/geocode/','');
    
    // execute geocoding
    $xml = $GoogleMapsXML->readXML($location,$country);

    if($xml && "OK"==$xml->status)
    { 
        // Output the coordinates
        // list($longitude, $latitude, $altitude) = explode(",", $xml->Response->Placemark->Point->coordinates);
      $lat = $xml->result->geometry->location->lat;
      $lng = $xml->result->geometry->location->lng;

      $varValue = array("lat"=>($lat?$lat->__toString():""), "lng" => ($lng?$lng->__toString():""),"status"=>$xml->status);
    }
	else {
	  //$varValue = array("status"=>"FAILED","response"=>$xml);
	    $varValue = array("status"=>$xml->status,"error_message"=>$xml->error_message);
	}
    return $varValue;
  }

	public static function getMarkerIcon($icon)
	{

	if (!$icon) return null;
//		if (!is_numeric($icon))
//		{
//		    return $icon;
//		}
	 
//		$objFile = \FilesModel::findByPk($icon);
//		return $objFile->path;
		$iconstd = null;
        $iconTmp = $icon;
//		if (is_numeric($iconTmp = $icon))
//      {
         	$iconTmp = \FilesModel::findByPk($iconTmp);
//			$iconTmp = \FilesModel::findByUuid($iconTmp);
            if ($iconTmp->type == "file")
            {

                $iconstd =  $iconTmp->path;
            }
//       }
		return $iconstd;

	}

}
