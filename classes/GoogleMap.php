<?php

/**

 * PHP version 5
 * @copyright  Christian Muenster 2009 
 * @author     Christian Muenster 
 * @package    Controller
 * @license    LGPL 
 * @filesource
 */

/**
 * Class cm_GoogleMap
 *
 * @copyright  Christian Muenster 2009 
 * @author     Christian Muenster 
 * @package    cm_GoogleMap 
 *
 */
namespace ChrMue\cm_GoogleMaps;



class GoogleMap // extends Module
{
  public $BaseJsScript;
  public $MainJsScript;

  public $baseJsTemplate='cm_googlemap_js_base';
  public $mainJsTemplate='cm_googlemap_js_main';
  public $routeDefJsTemplate='cm_googlemap_js_routedef';
  public $routeInitJsTemplate='cm_googlemap_js_routeinit';

  public $language = 'de';

  public $settings=array();

  public $items = array();
  public $markerData = array();
  public $circleData = array();
  public $marker;
  public $template;
  public $singleView;
  public $layout;
  
  public function __construct($singleView)
  {
    $this->singleView=$singleView;
  }
  
  public function generate() {

    $this->template= new \FrontendTemplate($this->mainJsTemplate);

    $detailUrl="";
    $this->template->settings=$this->settings;
    $this->template->singleView=$this->singleView;

    if (!$this->singleView)
    {
	  if (count($this->markerData)>0)
      {
	    foreach ($this->markerData as $item)
	    {
	      $items[] = array(
	        'data' => $item['data'],
	        'infotext' => $item['infotext'],
	        'cm_coords' => $item['cm_coords'],
	        'icon' => $item['icon'],
	        'color' => $item['color'],
	        'url' => $item['url'],
	        'clickable' => $item['clickable']
	      );
		}
      }
      $this->template->items=$items;
      if ($this->circleData)
        $this->template->circleData=$this->circleData;
    }
    else
    {
      if ($this->settings['routeToDetail'])
      {
        $tmpl= new \FrontendTemplate($this->routeDefJsTemplate);
        $this->template->routeDefScript=$tmpl->parse();
      }
      $item = array(
        'infotext' => $this->marker['infotext'],
        'cm_coords' => $this->marker['cm_coords'],
        'icon' => $this->marker['icon'],
        'color' => $this->marker['color'],
        'clickable' => $this->marker['clickable']
      );
	  if ($this->showAdditionalItem && $this->additionalMarker)
	  {
		$additionalItem = array(
			'infotext' => $this->additionalMarker['infotext'],
			'cm_coords' => $this->additionalMarker['cm_coords'],
			'icon' => $this->additionalMarker['icon'],
			'color' => $this->additionalMarker['color'],
			'clickable' => $this->additionalMarker['clickable']
		);
	  }
	        
	  $this->template->item=$item;
	  $this->template->additionalItem=$additionalItem;
    }
	$this->template->showAdditionalItem = $this->showAdditionalItem && $this->additionalMarker;
    $this->template->layout=$this->layout;
  }

   public function parse() {
       //    echo $this->template->parse();
     return $this->template->parse();
   }
}