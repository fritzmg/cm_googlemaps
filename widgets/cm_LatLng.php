<?php

/**
 * Copyright (c) 2013 Christian Muenster
 *
 * @package cm_googlemaps
 * @link    https://contao.org/de/extension-list.html
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ChrMue\cm_GoogleMaps;


/**
 * Class cm_LatLng
 *
 * Provide methods to select a location on a map.
 * @copyright  Christian Muenster 2013
 * @author     Christian Muenster 
 * @package    cm_membergooglemaps
 */
class cm_LatLng extends \Widget
{

	/**
	 * Submit user input
	 * @var boolean
	 */
	protected $blnSubmitInput = true;

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'be_widget';

	/**
	 * Load the database object
	 * @param array
	 */
	public function __construct($arrAttributes=null)
	{
		$this->import('Database');
		parent::__construct($arrAttributes);

		$this->strOrderField = $GLOBALS['TL_DCA'][$this->strTable]['fields'][$this->strField]['eval']['orderField'];
		$this->blnIsMultiple = $GLOBALS['TL_DCA'][$this->strTable]['fields'][$this->strField]['eval']['multiple'];

		// Prepare the order field
		if ($this->strOrderField != '')
		{
			$this->strOrderId = $this->strOrderField . str_replace($this->strField, '', $this->strId);
			$this->strOrderName = $this->strOrderField . str_replace($this->strField, '', $this->strName);

			// Retrieve the order value
			$objRow = $this->Database->prepare("SELECT {$this->strOrderField} FROM {$this->strTable} WHERE id=?")
						   ->limit(1)
						   ->execute($this->activeRecord->id);

			$this->{$this->strOrderField} = $objRow->{$this->strOrderField};
		}
	}


	/**
	 * Return an array if the "multiple" attribute is set
	 * @param mixed
	 * @return mixed
	 */
	protected function validator($varInput)
	{
 		return $varInput;
	}


	/**
	 * Generate the widget and return it as string
	 * @return string
	 */
	public function generate()
	{
		// Load the fonts for the drag hint (see #4838)
		$GLOBALS['TL_CONFIG']['loadGoogleFonts'] = true;

		$return = '<input type="hidden" name="'.$this->strName.'" id="ctrl_'.$this->strId.'" value="'
//		.implode(',', $arrSet)
		.$this->varValue
		.'">' . (($this->strOrderField != '') ? '<input type="hidden" name="'
		.$this->strOrderName.'" id="ctrl_'.$this->strOrderId.'" value="'
   		.$this->varValue
		.$this->{$this->strOrderField}.'">' : '') 
;
$return=""
		.'<div class="selector_container wizard">' 
		.(($this->strOrderField != '' && count($arrValues)) ? '<p class="sort_hint">' 
		. $GLOBALS['TL_LANG']['MSC']['dragItemsHint'] . '</p>' : '') 
		.'<input id="ctrl_'.$this->strId.'" type="text" name="'.$this->strName.'" class="'
		.trim((($this->strOrderField != '') ? 'sortable ' : '').($this->blnIsGallery ? 'sgallery' : ''))
		.' tl_text" value="'
    	.$this->varValue
    	.'" >';

		$return .= ' <p class ="coord_picker"><a href="system/modules/cm_googlemaps/widgets/cm_MapPanel.php?do='.\Input::get('do')
			.'&amp;table='.$this->strTable.'&amp;field='.$this->strField
			.'&amp;act=show&amp;id='.\Input::get('id')
//			.'&amp;value='.'abc8.123456,51.1213456'
			.'&amp;value='.$this->varValue
			.'&amp;rt='.REQUEST_TOKEN.'" class="tl_submit coord_picker" onclick="Backend.getScrollOffset();cm_Backend.openModalSelector({\'width\':765,\'title\':\''.specialchars($GLOBALS['TL_LANG']['MSC']['coordspicker']).'\',\'url\':this.href,\'id\':\''
			.$this->strId.'\'});return false">'.
			'<span>'.$GLOBALS['TL_LANG']['MSC']['pickCoords'].'>'
			.'</span></a></p></div>';


		if (!\Environment::get('isAjaxRequest'))
		{
			$return = '<div>' . $return . '</div>';
		}

		return $return;
	}
}
