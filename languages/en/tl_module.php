<?php 
/**
 * TL_ROOT/system/modules/cm_googlemaps/languages/de/tl_module.php 
 * 
 * Contao extension: cm_googlemaps
 * 
 * Copyright : © 2018 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle 
 * 
 */

$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['0']="Cluster Markers";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['1']="Cluster Markers which are closed together";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['0']="Cluster size";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['1']="Enlarge this value will cluster more markers.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['0']="max. zoom factor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['1']="markers will bes clustered up to this zoom factor only.";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['0']="The name is required";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['1']="If the form to send an email ist visible, the user must fill out a name field to send a mail to the selected member.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid']['0']="Cluster Layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid']['1']="choose a Cluster Layout.";

?>
