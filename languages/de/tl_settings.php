<?php
/**
 * 
 * Contao extension: cm_googlemaps 
 * 
 * @copyright  Christian Muenster 2017
 * @author     Christian Muenster
 * @license    LGPL
 * @filesource
 * 
 */
 
$GLOBALS['TL_LANG']['tl_settings']['cm_googlemaps'] = "Google Maps";
$GLOBALS['TL_LANG']['tl_settings']['cm_requestlimit'][0] = "Max. Anzahl der Anfragen je Adresse";
$GLOBALS['TL_LANG']['tl_settings']['cm_requestlimit'][1] = "Bitte geben Sie an, wie of maximal versucht werden soll, zu einer -Adresse bei Google die Koordinaten zu ermitteln.";
$GLOBALS['TL_LANG']['tl_settings']['cm_request_gm_ssl'][0] = "GoogleMaps-Abfragen immer mit SSL";
$GLOBALS['TL_LANG']['tl_settings']['cm_request_gm_ssl'][1] = "Bitte geben Sie an, ob GoogleMaps-Abfragen immer mit ssl erfolgen sollen.";
$GLOBALS['TL_LANG']['tl_settings']['cm_map_apikey']= array('Google API Key', 'Bitte geben Sie den BrowserKey, den Sie von Google unter <a href="http://code.google.com/apis/maps/signup.html">http://code.google.com/apis/maps/signup.html</a> erhalten haben.');
 

?>
